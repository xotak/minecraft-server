FROM alpine:latest

RUN apk add --update --no-cache openjdk8-jre curl tar
RUN curl -k https://framagit.org/loulou310/minecraft-server/-/archive/main/minecraft-server-main.tar.gz -o server.tar.gz 
RUN tar -xvzf server.tar.gz
RUN chmod +x /minecraft-server-main/docker-startup.sh

EXPOSE 25565/tcp

ENTRYPOINT ./minecraft-server-main/docker-startup.sh
